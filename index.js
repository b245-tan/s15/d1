// [Section] Comments

// Comments are parts of the code that gets ignored by the language.
// Comments are meant to describe the written code.

// There are two types of comments"
// - Single line comment (//)
/*
	-Multiline comment (/ * * /)
	-ctrl shift /
*/

// [SECTION] Statements and Syntax
	// Statements
		// In programming language, statements are instructions that we tell the computer to perform.
		// JS Statements usually ends with semicolon (;).
			// Also called as delimeter.
		// Semicolons are not required in JS.
			// it is used to help us train or locate where a statement ends.
	
	// Syntax
		// In programming, it is the set of rules that describes how statements must be constructed.

console.log("Hello World");

// [SECTION] Variables

// It is used to contain data.

// Declaring a variable
	// tells our devices that a variable name is created and is ready to store data.

	// Syntax: let/const variableName
		// let is a keyword that is usually used in declaring a variable
		let myVariable;
		console.log(myVariable); //useful for printing values of a variable.

		// console.log(Hello); //error: not defined

		// Initialize a value
			// Storing the initial value of a variable.
			// Assignment Operator (=)
		myVariable = "Hello";

		// Reassignment of variable value
			// Changing the initial value to a new value
		myVariable = "Hello World"
		console.log(myVariable);

		// const
			// "const" keyword is used to declare and initialize a constant variable.
			// A "constant" variable
		// const PI;
		// PI = 3.1416;
		// console.log(PI); //error: Initial value is missing for const.

	// Declaring with Initialization
		// a variable is given its initial/starting value upon declaration.
			// Syntax: let/const variableName = value;

			// let keyword
			let productName = "Desktop Computer";
			console.log(productName);

			let productPrice = 18999;
			// let productPrice = 20000; //error: variable has been decalred already
			productPrice = 20000;
			console.log(productPrice);

			// const keyword
			const PI = 3.1416;
			// PI = 3.14; //error: assignment to constant variable.
			console.log(PI);

/*
	Guide in writing variables
	1. Use the "let" keyword if the variable will contain different values/can change over time.
		Naming Convention: variable name should start with lowercase characters and use camelCasing for multiple words.

	2. Use the "const" if the variable does not change.
		Naming Convention: "const" keyword should be followed by all CAPITAL VARIABLE NAME (single value variable).

	3. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

	4. Variable names are case sensitive.

*/

// Multiple Variable Declaration

			let productCode = "DC017", productBrand = "Dell";
			console.log(productCode, productBrand);

// Using a reserved keyword

			// const let = "hello"
			// console.log(let); //error: lexically bound. "let" is a reserved keyword

// [SECTION] Data Types

// In Javascript, there are six types of data
	
	// String
		// are series of characters that create a word, phrase, sentence, or anything related to creating texts.
		// Strings in JavaScript can be written using a single quote ('') or double quote ("")

		let country	= "Philippines";
		let province = 'Metro Manila';

	// Concatenating Strings
		// multiple string values can be combined to create a single string using the "+" symbol.

		// I live in Metro Manila, Philippines
		let greeting = "I live in " + province + ", " + country
		console.log(greeting);

		// Escape Characters
			// (\) in string combination with other characters can produce different result
			// "\n" refers to creating a new line.
		let mailAddress = "Quezon City\nPhilippines";
		console.log(mailAddress);

		// Expected output: John's employees went home early.
		let message = "John's employees went home early.";
		console.log(message);

		//using the escape characters (\)
		message = 'Jane\'s employees went home early.';
		console.log(message);

	// Numbers

		// Integers/Whole Numbers
		let headcount = 26;
		console.log(headcount);

		// Decimal Numbers/Fractions
		let grade = 98.7;
		console.log(grade);

		// Exponential Notation
		let planetDistance = 2e10;
		console.log(planetDistance);

		// Combine numbers and strings
		console.log("John's grade last quarter is " + grade);

		// Boolean
			// Boolean values are normally used to store values relating to the state of certain things.
			// true or false (two logical values)

			let isMarried = false;
			let isGoodConduct = true;

			console.log("ismarried: " +isMarried);
			console.log("isGoodConduct: " +isGoodConduct);

		// Arrays
			// are special kind of data type that can be used to store multiple related values.
			// Syntax: let/const arrayName = [elementA, elementB,..., elementNth];

			const grades = [98.7, 92.1, 90.2, 94.6];
			// constant variable cannot be reassigned.
			// grades = 100; not allowed
			grades[0] = 100; //reference/index value
			console.log(grades);

			// This will work, but not recommended.
			let details = ["John", "Smith", 32, true];
			console.log(details);

		// Objects
			// Objects are another special kind of data type that is used to mimic real world objects/items.
			// Used to create complex data that contains information relevant to each other.
			/*
				Syntax:
				 let/const objectName = {
				 	propertyA: valueA
				 	propertyB: valueB
				 }
			*/

			let person = {
				fullName: "Juan Dela Cruz",
				age: 35,
				isMarried: false,
				contact: ["+63 912 345 6789", "8123 456"],
				address: {
					houseNumber: "345",
					city: "Manila"
				}
			};
			console.log(person);

			// Also useful for creating abstract objects
			let myGrades = {
				firstGrading: 98.7,
				secondGrading: 92.1,
				thirdGrading: 90.2,
				fourthGrading: 94.6,
			};
			console.log(myGrades);

			// type of operator is used to determine the type of data or the value of a variable.

			console.log(typeof myGrades); //oject

			// arrays is a special type of object with methods.
			console.log(typeof grades); //object

	// Null
		// indicates the absence of value.
		let spouse = null;
		console.log(spouse);

	// Undefined
		// indicates that a variable has not been given a value yet.
		let fullName;
		console.log(fullName);